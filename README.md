![PortadaProgramaWEB](http://www.madarme.co/portada-web.png)
# Título del proyecto:

## Casos reportados de Covid19 del mes de Marzo en Colombia.

## Índice 📜
1. [Características](#-características)
2. [Contenido del proyecto](#-contenido-del-proyecto)
3. [Tecnologías](#%EF%B8%8F-tecnologías-)
4. [IDE](#%EF%B8%8F-ide)
5. [Instalación](#%EF%B8%8F-instalación)
6. [Demo](#-demo)
7. [Autor](#-autor)
8. [Institución Académica](#-institución-académica)

## 💡 Características: 

- Manejeno de Google Charts
- Creación de piechart y tablas de Google Charts
- Creación dinámica de componentes de HTML con Javascript
- Lectura de datos json a través de la API fecth JavaScript
- Carga dinámica del JSON 
- Archivo json de ejemplo: [ver](https://www.datos.gov.co/resource/gt2j-8ykr.json)


## 📝 Contenido del proyecto:
- [index.html](<index.html>): Archivo principal de la página web. Donde se puede ver los totales de los casos a nivel nacional registrados en el mes de marzo. Hay 3 sesiones:
1. Casos por genero a nivel nacional, donde se encuentra un vinculo que redirecciona a ref1.html.
2. Casos por fuente de contagio a nivel nacional, donde se encuentra un vinculo que redirecciona a ref2.html.
3. Muestra el numero total de departamentos que reportaron casos y un se encuentra un vinculo que redirecciona a ref3.html.

- [ref1.html](<html/ref1.html>): Archivo donde se muestra el total de casos reportados por genero en cada departamento, en este archivo muestro una grafica o piechart con los totales correspondientes según el tipo de género o sexo.
- [ref2.html](<html/ref2.html>): Archivo donde se muestra el total de casos reportados por fuente de contagio (Importado - Relacionado) en cada municipio, en este archivo muestro una grafica o piechart con los totales correspondientes según el tipo de contagio.
- [ref3.html](<html/ref3.html>): Archivo donde se muestra el total de casos reportados por departamento, en este archivo se muestra una tabla y una grafuca donde se muestran los totales correspondientes de cada uno de los departamentos.
- [js/funciones.js](<js/funciones.js>): Archivo que contiene la lógica de la página donde estan todas las funciones como lectura de JSON y la creación de las graficas o chart con la ayuda de la API de Google, Google Charts.


## 🛠️ Tecnologías :

- Google Charts
- Bootstrap 4.4
- CSS 3
- HTML 5
- JavaScript


## 🖥️ IDE:

El proyecto se desarrolla usando visual studio code. 


## ⚙️ Instalación:

Se necesita un navegador web para utilizar la página web. Se recomienda google chrome, microsoft edge o firefox.

Para poner en funcionamiento la Página tiene que ingresar a index.html.

## 🔍 Demo:
Para ver el demo de la aplicación puede dirigirse a: [Covid19ColombiaMarzo](http://ufps24.madarme.co/covid19colombiamarzo/).

## Ejemplo de funcionamiento:

### Muestra de todos los casos reportados en todo el país:
![Index](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/covid19ColombiaMarzo/index.PNG)
***

### Muestra de todos los casos reportados por género según la selección de un departamento:
![Ref1](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/covid19ColombiaMarzo/ref1.PNG)
***

### Muestra de todos los casos reportados por fuente de contagio (Importado - Relacionado) según la selección de un municipio:
![Ref2](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/covid19ColombiaMarzo/ref2.PNG)
***

### Muestra el total de casos reportados de cada departamento:
![Ref3-1](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/covid19ColombiaMarzo/ref3-1.PNG)

![Ref3-2](https://gitlab.com/martinjesusmr/pantallazos/-/raw/master/covid19ColombiaMarzo/ref3-2.PNG)


## 🧑 Autor:
Proyecto desarrollado por [Martin de Jesus Medina Ruvian](<https://martinmedinaruvian.github.io/index/>).
Código: 1151791.

## 🏫 Institución Académica:  
Proyecto desarrollado en la Materia programación web del Programa de Ingeniería de Sistemas de la Universidad Francisco de Paula Santander.