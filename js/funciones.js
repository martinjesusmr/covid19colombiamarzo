var datos;

function obtenerArregloSinRepetir(arreglo){
  let arregloSinRepetir = new Set(arreglo);
  arregloSinRepetir = Array.from(arregloSinRepetir);
  arregloSinRepetir.sort();
  return arregloSinRepetir;
}

cargarChartGoogle();

function cargarChartGoogle(){
  google.charts.load('current', {'packages':['table']});
  google.charts.load('current', {'packages':['corechart']});
}

async function leerJSON(){
  try {
     let respuesta = await fetch('https://martinmedinaruvian.github.io/json/covid19ColombiaMarzo.json');
     datos = await respuesta.json();
  } catch (error) {
    console.log(error)
  }
}

async function obtenerDatosJSON(){
  await leerJSON();
}

async function obtenerDatosPais(){
  await obtenerDatosJSON();
  mostrarTotalCasos();
  verPieChartGeneral(1, 'Total de casos por género reportados en todo el pais', null, 'Hombres', 'Mujeres', total(3, 'M', null, null, null),  total(3, 'F', null, null, null), document.querySelector('#pie-chart-genero-pais'));
  verPieChartGeneral(1, 'Total de casos por fuente de contagio reportados en todo el pais', null, 'Importados', 'Relacionados', total(4, null, null, null, 'Importado'),  total(4, null, null, null, 'Relacionado'), document.querySelector('#pie-chart-fuente-contagio-pais'));
}

async function obtenerDatosGeneroDepartamento(){
  await obtenerDatosJSON();
  mostrarDepartamentosSelect();
  mostrarTotalCasosGeneroDepartamentos();
}

async function obtenerDatosFuenteContagioMunicipio(){
  await obtenerDatosJSON();
  mostrarMunicipiosSelect();
  mostrarTotalCasosFuenteContagioMunicipio();
}

function total(tipo, genero, departamento, municipio, fuenteContagio){
  let total = 0;
  for(let dato of obtenerArregloSinRepetir(this.datos)){
  if(tipo == 1){ 
    if(dato.departamento_nom == departamento && dato.sexo == genero){
      total ++;
    }
  }else if(tipo == 2){
    if(dato.ciudad_municipio_nom == municipio && dato.fuente_tipo_contagio == fuenteContagio){
      total ++;
    }
  }else if(tipo == 3){
    if(dato.sexo == genero){
      total ++;
    }
  }else if(tipo == 4){
    if(dato.fuente_tipo_contagio == fuenteContagio){
      total ++;
    }
  }else if(tipo == 5){
    if(dato.departamento_nom == departamento){
      total ++;
    }
  }
  
  }

  return total;
}

function totalCasosHombresPais(){
  return total(3, 'M', null, null, null);
}

function totalCasosMujeresPais(){
  return datos.length - totalCasosHombresPais();
}

function mostrarTotalCasosGenerosPais(){
  document.querySelector('#total-casos-pais-hombres').innerHTML = totalCasosHombresPais();
  document.querySelector('#total-casos-pais-mujeres').innerHTML = totalCasosMujeresPais();
}

function mostrarTotalCasosFuenteContagioPais(){
  document.querySelector('#total-casos-pais-importados').innerHTML = totalCasosImportadosPais();
  document.querySelector('#total-casos-pais-relacionados').innerHTML = totalCasosRelacionadoPais();
  document.querySelector('#total-casos-fuente-contagios').innerHTML = totalCasosRelacionadoPais() + totalCasosImportadosPais();
}

function mostrarTotalCasos(){
  document.querySelector('#total-casos').innerHTML = datos.length;
  document.querySelector('#total-departamentos').innerHTML = obtenerArregloSinRepetir(obtenerDatosParaSelect(1)).length;
  mostrarTotalCasosGenerosPais();
  mostrarTotalCasosFuenteContagioPais();
}

function obtenerDatosParaSelect(tipo){
  let datos = [];
  for(let dato of obtenerArregloSinRepetir(this.datos)){
    if(tipo == 1){
      datos.push(dato.departamento_nom);
    }else if(tipo == 2){
      datos.push(dato.ciudad_municipio_nom);
    }
  }
  return datos;
}

function formatoOpcionesSelect(datos){
  let msg = '';
  let datosOrganizados = obtenerArregloSinRepetir(datos);
  for(let i=0; i < datosOrganizados.length; i++){
    msg += `<option value='${datosOrganizados[i]}'>${datosOrganizados[i]}</option>` 
  }
  return msg
}

function mostrarDepartamentosSelect(){
  document.querySelector('#cmbdepartamentos').innerHTML = formatoOpcionesSelect(obtenerDatosParaSelect(1));
}

function mostrarMunicipiosSelect(){
  document.querySelector('#cmbmunicipios').innerHTML = formatoOpcionesSelect(obtenerDatosParaSelect(2));
}

function totalCasosImportadosPais(){
  return total(4, null, null, null, 'Importado');
}

function totalCasosRelacionadoPais(){
  return total(4, null, null, null, 'Relacionado');
}

function obtenerDatosPorFiltro(tipo, departamento, municipio){
  let datos = [];
  for(let dato of obtenerArregloSinRepetir(this.datos)){
  if(tipo == 1){
    if(dato.departamento_nom == departamento){
      datos.push(dato);
    }
  }else if(tipo ==2){
    if(dato.ciudad_municipio_nom == municipio){
      datos.push(dato);
    }
  }
  }
  return datos;
}

function mostrarTotalCasosGeneroDepartamentos(){
  let select = document.querySelector('#cmbdepartamentos');
  var departamento = select.options[select.selectedIndex].value;

  document.querySelector('#departamento-seleccionado').innerHTML = departamento;
  let totalCasosDepartamento = obtenerDatosPorFiltro(1, departamento, null).length;

  document.querySelector('#total-casos-departamento').innerHTML = totalCasosDepartamento;
  
  let totalCasosHombres = total(1, 'M', departamento, null, null);
  document.querySelector('#total-casos-departamento-hombres').innerHTML = totalCasosHombres;

  let totalCasosMujeres = (totalCasosDepartamento - totalCasosHombres);
  document.querySelector('#total-casos-departamento-mujeres').innerHTML = totalCasosMujeres;

  verPieChartGeneral(1, 'Total de casos por género reportados en ', departamento, 'Hombres', 'Mujeres', totalCasosHombres,  totalCasosMujeres, document.querySelector('#pie-chart-genero-departamento'));
  crearTablaGeneral(1, "Hombres", "Mujeres", totalCasosHombres, totalCasosMujeres, document.querySelector('#tabla-genero-departamento'));
}

function mostrarTotalCasosFuenteContagioMunicipio(){  
  let select = document.querySelector('#cmbmunicipios');
  var municipio = select.options[select.selectedIndex].value;

  document.querySelector('#municipio-seleccionado').innerHTML = municipio;
  let totalCasosMunicipio = obtenerDatosPorFiltro(2, null, municipio).length;

  document.querySelector('#total-casos-municipio').innerHTML = totalCasosMunicipio;
  
  let totalCasosRelacionados = total(2, null, null, municipio, 'Relacionado');
  document.querySelector('#total-casos-municipio-relacionados').innerHTML = totalCasosRelacionados;

  let totalCasosImportados = total(2, null, null, municipio, 'Importado');
  document.querySelector('#total-casos-municipio-importados').innerHTML = totalCasosImportados;

  verPieChartGeneral(2, 'Total de casos por fuente de contagio reportados en ', municipio, 'Importados', 'Relacionados', totalCasosImportados,  totalCasosRelacionados, document.querySelector('#pie-chart-fuente-contagio-municipio'));
  crearTablaGeneral(2, "Importados", "Relacionados", totalCasosImportados, totalCasosRelacionados, document.querySelector('#tabla-fuente-contagio-municipio'));
}

function crearEncabezadoPieChartGeneral(tipo, data){
  if(tipo == 1){
    data.addColumn('string', 'Fuente');
    data.addColumn('number', 'Total');
  }else if(tipo == 2){
    data.addColumn('string', 'Genero');
    data.addColumn('number', 'Total');
  }
}

function crearPieChartGeneral(data, titulo, descripcionTitulo, sesionDOM){
  let pieChart = new google.visualization.PieChart(sesionDOM);
  opcionesPieChart = {title:titulo + ' ' + descripcionTitulo, is3D:true};
  pieChart.draw(data, opcionesPieChart);
}

function verPieChartGeneral(tipo, titulo, descripcionTitulo, descripcion1, descripcion2, total1, total2, sesionDOM){ 
  let data = new google.visualization.DataTable();

  crearEncabezadoPieChartGeneral(tipo, data);

  data.addRows(2);
  
  data.setCell(0,0, descripcion1);
  data.setCell(0,1, total1);
  data.setCell(1,0, descripcion2);
  data.setCell(1,1, total2);
  
  crearPieChartGeneral(data, titulo, descripcionTitulo, sesionDOM);
}

function crearEncabezadosTablaGeneral(tipo, data) {
  if(tipo == 1){
    data.addColumn('string', 'Género');
    data.addColumn('number', 'Total');
  }else if(tipo == 2){
    data.addColumn('string', 'Fuente de contagio');
    data.addColumn('number', 'Total');
  }else if(tipo == 3){
    data.addColumn('string', 'DEPARTAMENTO');
    data.addColumn('number', 'TOTAL');
  }
}

function totalPorDepartamento(){
  let datos = new Array(2);
  let nombreDepartamentos = [];
  let totalCasosDepartamentos = [];
  let departamentosSinRepetir = obtenerArregloSinRepetir(obtenerDatosParaSelect(1));
  for(let i=0; i < departamentosSinRepetir.length; i++){
    nombreDepartamentos.push(departamentosSinRepetir[i]);
    totalCasosDepartamentos.push(total(5, null, departamentosSinRepetir[i], null, null));
  }

  datos[0] = nombreDepartamentos;
  datos[1] = totalCasosDepartamentos;

  return datos;
}

function crearTabla(data){
  let estilosTabla = {headerCell: 'bg-primary text-white text-center p-2', hoverTableRow: 'bg-warning', tableCell:'text-center p-2', hoverRowNumberCell: 'text-white'};
  var table = new google.visualization.Table(document.querySelector('#tabla-total-departamentos'));
  let opcionesTabla = {showRowNumber: true, width: '100%', height: '100%', cssClassNames: estilosTabla};
  table.draw(data, opcionesTabla);
}

async function verTablaTotalDepartamentos(){
  await obtenerDatosJSON();
 
  var data = new google.visualization.DataTable();
  
  crearEncabezadosTablaGeneral(3, data);

  let totalDepartamentos = totalPorDepartamento()[0].length;
  data.addRows(totalDepartamentos);
 
  for(let i=0; i < totalDepartamentos; i++){
    data.setCell(i, 0, totalPorDepartamento()[0][i] + "");  
    data.setCell(i, 1, totalPorDepartamento()[1][i]);  
  }
  crearTabla(data);
  crearLineChart(data);
}

function crearLineChart(data){
  var table = new google.visualization.PieChart(document.querySelector('#line-chart-total-departamentos'));
  var opcionesLineChart = {title: 'Grafica de total de casos reportados por cada departamento:', width: '100%', height:'100%'};
  table.draw(data, opcionesLineChart);
}

function crearTablaGeneral(tipoEncabezado, tipo1, tipo2, casos1, caso2, sesionDOM) {
  var data = new google.visualization.DataTable();

  crearEncabezadosTablaGeneral(tipoEncabezado, data);

  data.addRows(2);

  data.setCell(0, 0, tipo1);
  data.setCell(0, 1, casos1);
  data.setCell(1, 0, tipo2);
  data.setCell(1, 1, caso2);

  var table = new google.visualization.Table(sesionDOM);

  let estilosTabla = {headerCell: 'bg-dark text-white text-center p-2', hoverTableRow: 'bg-warning', tableCell:'text-center p-2', hoverRowNumberCell: 'text-white'};
  let opcionesTabla = {showRowNumber: true, width: '100%', height: '100%', cssClassNames: estilosTabla};
  table.draw(data, opcionesTabla);
}

